EIP=52.220.233.38
SSH_KEY_PATH=/home/user/MEGA/Builds/build-1/deploy/terraform/amazon/docker/default.pem

key-permissions:
	chmod 600 ssh/default.pem

rsync-docker:
	rsync -avz -e 'ssh -i ${SSH_KEY_PATH}' /home/user/MEGA/Builds/build-1/deploy/ansible/roles/tools/docker ubuntu@${EIP}:/opt

rsync-rancher:
	rsync -avz -e 'ssh -i ${SSH_KEY_PATH}' /home/user/MEGA/Builds/build-1/deploy/ansible/roles/services/rancher ubuntu@${EIP}:/opt
