resource "aws_security_group" "ec2_allow_all" {

  name = "ec2_allow_all"
  description = "allow everything"
  vpc_id = "${var.default_vpc_id}"

  ingress {
      from_port = 1
      to_port = 65535
      protocol = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
      from_port = 1
      to_port = 65535
      protocol = "udp"
      cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
      from_port = 1
      to_port = 65535
      protocol = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
      from_port = 1
      to_port = 65535
      protocol = "udp"
      cidr_blocks = ["0.0.0.0/0"]
  }

}


resource "aws_security_group" "ec2_sg" {

  name = "ec2_sg"
  description = "ssh and http rules"
  vpc_id = "${var.default_vpc_id}"

  ingress {
      from_port = 22
      to_port = 22
      protocol = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
      from_port = 53
      to_port = 53
      protocol = "udp"
      cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
      from_port = 80
      to_port = 80
      protocol = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
      from_port = 443
      to_port = 443
      protocol = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
      from_port = 514
      to_port = 514
      protocol = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
      from_port = 3306
      to_port = 3306
      protocol = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
      from_port = 11371
      to_port = 11371
      protocol = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
      from_port = 1
      to_port = 65535
      protocol = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
      from_port = 1
      to_port = 65535
      protocol = "udp"
      cidr_blocks = ["0.0.0.0/0"]
  }

}

resource "aws_security_group" "rds_sg" {

  name = "rds_sg"
  description = "mysql and dns rules"
  vpc_id = "${var.default_vpc_id}"

  ingress {
      from_port = 53
      to_port = 53
      protocol = "udp"
      cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
      from_port = 3306
      to_port = 3306
      protocol = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
      from_port = 1
      to_port = 65535
      protocol = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
      from_port = 1
      to_port = 65535
      protocol = "udp"
      cidr_blocks = ["0.0.0.0/0"]
  }
}
