# AWS credentials
aws_access_key = "TF_VAR_AWS_ACCESS_KEY"
aws_secret_key = "TF_VAR_AWS_SECRET_KEY"

# Servername
server_name = "docker"
domain_1 = "TF_VAR_DOMAIN_1"

# VPC
# https://ap-southeast-1.console.aws.amazon.com/vpc/home?region=ap-southeast-1
default_vpc_id = "vpc-db39e3bf"
default_subnet_id = "subnet-c8870cbe"

# EC2
ec2_instance_type = "t2.micro"
ec2_region = "ap-southeast-1"
ec2_availability_zone = "ap-southeast-1a"

# Ami ID
# Ubuntu 16.04 LTS amd64 hvm:ebs-ssd
ec2_ami_id = "ami-a1288ec2"

# Packer / Debian / Ubuntu
#ec2_instance_user = "user"
#ec2_instance_user = "admin"
ec2_instance_user = "ubuntu"

# Keys
ec2_private_key_name = "default"
ec2_private_key_path = "TF_VAR_EC2_PRIVATE_KEY_PATH"

# EBS
ebs_block_device_name = "/dev/sdf"
ebs_volume_type = "standard"

# Sources
docker_source = "TF_VAR_DOCKER_SOURCE"
compose_source = "TF_VAR_COMPOSE_SOURCE"
rancher_source = "TF_VAR_RANCHER_SOURCE"
vault_source = "TF_VAR_VAULT_SOURCE"

# AWS RDS
# https://eu-west-1.console.aws.amazon.com/rds/home?region=eu-west-1#launch-dbinstance:ct=gettingStarted:
rds_engine = "mariadb"
rds_engine_version = "10.0.24"
rds_instance_class = "db.t2.micro"
rds_db_name = "websitecom"
rds_db_storage = "10"
rds_db_username = "TF_VAR_RDS_DB_USERNAME"
rds_db_password = "TF_VAR_RDS_DB_PASSWORD"
rds_availability_zone = "ap-southeast-1a"
rds_backup_window = "2:00-3:00"
rds_maintenance_window = "mon:4:00-mon:5:00"
rds_backup_retention_period = "7"
rds_final_snapshot_identifier = "final-snapshot"
