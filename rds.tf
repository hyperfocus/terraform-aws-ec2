resource "aws_db_instance" "db_1" {

  name = "${var.rds_db_name}"
  identifier = "${var.rds_db_name}"
  allocated_storage = "${var.rds_db_storage}"

  engine = "${var.rds_engine}"
  engine_version = "${var.rds_engine_version}"
  instance_class = "${var.rds_instance_class}"
  storage_type = "gp2"

  username = "${var.rds_db_username}"
  password = "${var.rds_db_password}"

  port = 3306
  publicly_accessible = true
  storage_encrypted = false
  apply_immediately = false
  multi_az = false

  parameter_group_name = "default.mariadb10.0"
  availability_zone = "${var.rds_availability_zone}"

  backup_window = "${var.rds_backup_window}"
  maintenance_window = "${var.rds_maintenance_window}"
  backup_retention_period = "${var.rds_backup_retention_period}"
  final_snapshot_identifier = "${var.rds_final_snapshot_identifier}"

}

# Configure the MySQL provider based on the outcome of
# creating the aws_db_instance.
#provider "mysql" {
#    endpoint = "${aws_db_instance.default.endpoint}"
#    username = "${aws_db_instance.default.username}"
#    password = "${aws_db_instance.default.password}"
#}

# Create a second database, in addition to the "initial_db" created
# by the aws_db_instance resource above.
#resource "mysql_database" "app" {
#    name = "another_db"
#}
