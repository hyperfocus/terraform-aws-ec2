#variable "aws_access_key" {}
#variable "aws_secret_key" {}

variable "domain_1" {}
variable "server_name" {}
variable "default_vpc_id" {}
variable "default_subnet_id" {}

variable "ec2_ami_id" {}
variable "ec2_instance_type" {}
variable "ec2_region" {}
variable "ec2_availability_zone" {}

variable "ec2_instance_user" {}

variable "ec2_private_key_name" {}
variable "ec2_private_key_path" {}

variable "ebs_block_device_name" {}
variable "ebs_volume_type" {}
#variable "ebs_snapshot_id" {}

variable "docker_source" {}
variable "compose_source" {}
variable "rancher_source" {}
variable "vault_source" {}

variable "rds_engine" {}
variable "rds_engine_version" {}
variable "rds_instance_class" {}

variable "rds_db_name" {}
variable "rds_db_storage" {}
variable "rds_db_username" {}
variable "rds_db_password" {}

variable "rds_availability_zone" {}
variable "rds_backup_window" {}
variable "rds_maintenance_window" {}
variable "rds_backup_retention_period" {}
variable "rds_final_snapshot_identifier" {}
