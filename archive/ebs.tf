  ebs_block_device {
    device_name = "${var.ebs_block_device_name}"
    volume_type = "${var.ebs_volume_type}"
	snapshot_id = "${var.ebs_snapshot_id}"
    volume_size = 30
    delete_on_termination = true
    encrypted = false
  }
