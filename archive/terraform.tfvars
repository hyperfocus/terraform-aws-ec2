# Atlas
atlas_token = "xx"

# Cloudflare
cloudflare_email = "xxx"
cloudflare_token = "xxx"

# AWS Elasticache
# https://eu-central-1.console.aws.amazon.com/elasticache/home?region=eu-west-1#
ec2_node_type = "cache.t2.micro"

# VPC
vpc_subnet_ip = "10.0.0.0/16"
subnet_1_ip = "10.0.1.0/24"
subnet_1_az = "ap-south-1a"
subnet_2_ip = "10.0.2.0/24"
subnet_2_az = "ap-south-1a"
subnet_3_ip = "10.0.3.0/24"
subnet_3_az = "ap-south-1b"
