resource "aws_elasticache_cluster" "memcached" {
    cluster_id = "memcached-node"
    engine = "memcached"
    node_type = "${var.ec_node_type}"
    port = 11211
    num_cache_nodes = 1
    parameter_group_name = "default.memcached1.4"
}