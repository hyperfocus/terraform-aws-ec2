  db_subnet_group_name = "${aws_db_subnet_group.rds_subnets.name}"
  vpc_security_group_ids =  ["${aws_security_group.rds_sg.id}"]

resource "aws_db_subnet_group" "rds_subnets" {
    name = "main"
    description = "main rds subnet group"
    subnet_ids = ["${aws_subnet.2.id}", "${aws_subnet.3.id}"]
}
