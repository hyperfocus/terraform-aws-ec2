variable "atlas_token" {}

variable "cloudflare_email" {}
variable "cloudflare_token" {}

variable "ec2_node_type" {}

variable "vpc_subnet_ip" {}

variable "subnet_1_ip" {}
variable "subnet_1_az" {}

variable "subnet_2_ip" {}
variable "subnet_2_az" {}

variable "subnet_3_ip" {}
variable "subnet_3_az" {}
