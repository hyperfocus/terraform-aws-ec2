resource "cloudflare_record" "domain_1_1" {
    domain = "${var.domain_1}"
    type = "A"
    name = "localhost"
    value = "127.0.0.1"
    ttl = 3600
}


resource "cloudflare_record" "domain_1_3" {
    domain = "${var.domain_1}"
    type = "CNAME"
    name = "www"
    value = "${var.domain_1}"
    ttl = 3600
}
