resource "aws_vpc" "default" {
  cidr_block = "${var.vpc_subnet_ip}"
  enable_dns_support = true
  enable_dns_hostnames = true
}


resource "aws_subnet" "1" {
  vpc_id = "${aws_vpc.default.id}"
  cidr_block = "${var.subnet_1_ip}"
  availability_zone = "${var.subnet_1_az}"
  tags {
      Name = "subnet_1"
  }
}

resource "aws_subnet" "2" {
  vpc_id = "${aws_vpc.default.id}"
  cidr_block = "${var.subnet_2_ip}"
  availability_zone = "${var.subnet_2_az}"
  tags {
      Name = "subnet_2"
  }
}

resource "aws_subnet" "3" {
  vpc_id = "${aws_vpc.default.id}"
  cidr_block = "${var.subnet_3_ip}"
  availability_zone = "${var.subnet_3_az}"
  tags {
      Name = "subnet_3"
  }
}


resource "aws_route_table" "1" {
  vpc_id = "${aws_vpc.default.id}"
}

resource "aws_route_table" "2" {
  vpc_id = "${aws_vpc.default.id}"
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = "${aws_internet_gateway.default.id}"
  }
}

resource "aws_main_route_table_association" "default" {
    vpc_id = "${aws_vpc.default.id}"
    route_table_id = "${aws_route_table.1.id}"
}



resource "aws_internet_gateway" "default" {
  vpc_id = "${aws_vpc.default.id}"
}

resource "aws_route_table_association" "1" {
  subnet_id = "${aws_subnet.1.id}"
  route_table_id = "${aws_route_table.2.id}"
}

resource "aws_route_table_association" "2" {
  subnet_id = "${aws_subnet.2.id}"
  route_table_id = "${aws_route_table.2.id}"
}

resource "aws_route_table_association" "3" {
  subnet_id = "${aws_subnet.3.id}"
  route_table_id = "${aws_route_table.2.id}"
}
