resource "aws_elasticache_cluster" "redis" {
    cluster_id = "redis-node"
    engine = "redis"
    node_type = "${var.ec_node_type}"
    port = 6379
    num_cache_nodes = 1
    parameter_group_name = "default.redis2.8"
}