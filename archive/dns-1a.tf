resource "cloudflare_record" "domain_1_2" {
    domain = "${var.domain_1}"
    type = "A"
    name = "${var.domain_1}"
    value = "${aws_instance.webserver_1.public_ip}"
    ttl = 3600
}
