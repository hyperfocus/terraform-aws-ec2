#!/bin/bash

# Install
sudo apt update
sudo apt install -y python-pip python-paramiko python-yaml python-jinja2 python-httplib2 python-six python-setuptools make git wget sshpass
sudo pip install --upgrade pip
sudo pip install ansible
